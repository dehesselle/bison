# bison for macOS

Build [`bison`](https://www.gnu.org/software/bison/) without any package managers.

## License

[GPL-2.0-or-later](LICENSE)

